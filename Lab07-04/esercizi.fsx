﻿let myconcat l = List.foldBack (@) l [];

#r "FsCheck"
open FsCheck;;

do Check.Quick (fun l -> List.concat l = myconcat l);;

let myfilter pred l = List.foldBack (fun el acc -> if pred el then el::acc else acc) l [];;

do Check.Quick (fun pred l -> List.filter pred l = myfilter pred l);;

let rec myreduceBack f l = 
    match l with
        | [] -> raise (System.ArgumentException("The input list was empty.\nParameter name: list"))
        | [el] -> el
        | h::tl -> f h (myreduceBack f tl);;

let mylast l = myreduceBack (fun _ acc -> acc) l;;

let rec prop_reduce f l =
    match l with
        | [] -> true
        | _ -> myreduceBack f l = List.reduceBack f l;;

do Check.Quick prop_reduce;;

let myreduceBack2 f l = List.foldBack f (l |> List.rev |> List.tail |> List.rev) (List.last l);;