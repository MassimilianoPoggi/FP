﻿let rec map f sq = 
    match Seq.isEmpty sq with
        | true -> Seq.empty
        | false -> seq { yield f (Seq.head sq); yield! map f (Seq.tail sq) }

let squares = Seq.initInfinite id |> map (fun x -> x*x) |> Seq.take 15 |> Seq.toList;;

let rec filter f sq =
    match Seq.isEmpty sq with
        | true -> Seq.empty
        | false when f (Seq.head sq) -> seq { yield Seq.head sq; yield! filter f (Seq.tail sq) }
        | _ -> seq { yield! filter f (Seq.tail sq) }

let threeMults = Seq.initInfinite id |> filter (fun x -> x % 3 = 0) |> Seq.take 20 |> Seq.toList;;

let rec fibFrom (a: uint64) b = seq { yield a; yield! fibFrom b (a+b) }

let fib n = Seq.item n (fibFrom 1UL 1UL);;

let rec sumSeq sq = seq { 
    yield Seq.head sq; 
    let newSeq = Seq.append [Seq.item 0 sq + Seq.item 1 sq] (Seq.skip 2 sq)
    in yield! sumSeq newSeq; }

let sumSeq2 sq =
    let rec sumSeq index sq = 
        seq { yield Seq.reduce (+) (Seq.take index sq); yield! sumSeq (index+1) sq }
    in sumSeq 1 sq;;

let triangulars = Seq.initInfinite id |> sumSeq |> Seq.take 15 |> Seq.toList;;