﻿//esercizio 1:
let f x = x + 1;;
let g x = x  +1;;
//f è una funzione che va da x in x
//g è una funzione che prende una funzione che va da int ad un tipo alfa qualsiasi e restituisce un alfa
//esempio di utilizzo per g:
let z l i = List.map (fun el -> el + i) l;;
let list = [1..15]
g (z list);;

let rec map f l =
    match l with
        | [] -> []
        | h::tl -> f h::map f tl;;

let l1 = [1..10];;
let l2 = map (fun x -> x*x) l1;;
let l3 = map (fun x -> if x%2 = 0 then (x, "pari") else (x, "dispari")) l1;;

let names = [ ("Mario", "Rossi") ; ("Anna Maria", "Verdi") ; ("Giuseppe", "Di Gennaro")] ;;
let names1 = map (fun (a,b) -> "Dott. " + a + " " + b) names;;

#r "FsCheck"
open FsCheck;;

let prop_map f (ls: int list) = 
    map f ls = List.map f ls;;

do Check.Quick prop_map;;

let prop_map_pres_len f (ls: int list) =
    List.length (map f ls) = List.length ls;;

do Check.Quick prop_map_pres_len;;

let rec filter pred ls = 
    match ls with
        | [] -> []
        | h::tl when pred h -> h::filter pred tl
        | h::tl -> tl;;

let mult3 n = filter (fun x -> x % 3 = 0) [1..n];;

let prop_filter pred (ls: int list) =
    filter pred ls = List.filter pred ls;;

do Check.Quick prop_filter;;

let prop_filter_len pred (ls: int list) =
    List.length (filter pred ls) <= List.length ls;;

do Check.Quick prop_filter_len;;

let rec filter1 pred ls =
    match ls with
        | [] -> ([], [])
        | h::tl when pred h -> let (l1,l2) = filter1 pred tl in (h::l1, l2)
        | h::tl -> let (l1,l2) = filter1 pred tl in (l1, h::l2);;

let multNonmult n = filter1 (fun x -> x % 3 = 0) [1..n];;
let p1 = multNonmult 20;;

let prop_filter1_len pred (ls: int list) =
    let (xs, ys) = filter1 pred ls in List.length(xs@ys) = List.length ls;;

do Check.Quick prop_filter1_len;;

let prop_filter1_app pred (ls: int list) =
    let (xs, ys) = filter1 pred ls in xs@ys = ls;;

do Check.Quick prop_filter1_app;;

let divisori n = List.filter (fun x -> n % x = 0) [1..n];;
let isPrime n = List.length (divisori n) = 2;;