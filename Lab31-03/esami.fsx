﻿type valV = { studente: string; voto: int }
type valG = { nome: string; giudizio: string }

let valuta v = 
    { nome = v.studente; giudizio = 
        match v.voto with
            | n when n < 18 -> "insufficiente"
            | n when n < 23 -> "sufficiente"
            | n when n < 28 -> "buono"
            | _ -> "ottimo"};;

let rec valutaList l =
    match l with
        | [] -> []
        | h::tl -> valuta h::valutaList tl;;

let valutaList2 l = List.map valuta l;;

#r "FsCheck"
open FsCheck;;

let prop_valutaList (xs: valV list) =
    List.map valuta xs = valutaList xs;;

do Check.Quick prop_valutaList;;

let rec creaValList (stud,voti) =
    match stud, voti with
        | [], _ | _, [] -> []
        | hs::ts, hv::tv -> {studente = hs; voto = hv}::creaValList(ts, tv);;

let creaValList2 (stud, voti) = 
    try 
        List.map2 (fun s v -> {studente=s; voto = v}) stud voti
    with
        //La map2 di f# non gestisce liste di lunghezza diversa
        //inoltro alla mia funzione
         :? System.ArgumentException -> creaValList(stud,voti);;

let media l = 
    let rec sommaAndConta list =
        match list with
            | [] -> (0,0)
            | h::tl -> let (s,c) = sommaAndConta tl in (s+h.voto, c+1)
    in match l with
       | [] -> 0.
       | _ -> let (sum, len) = sommaAndConta l in (float sum)/(float len);;

let media2 l = 
    match l with
        | [] -> 0.
        | _ -> List.fold (fun acc el -> acc + (float el.voto)) 0. l / (l |> List.length |> float);;

let media3 l = 
    match l with
        | [] -> 0.
        | _ -> List.average (List.map (fun el -> float el.voto) l);;

let rec separa l =
    match l with
        | [] -> ([], [])
        | h::tl when h.voto < 18 -> let (l1, l2) = separa tl in (h::l1, l2)
        | h::tl -> let(l1, l2) = separa tl in (l1, h::l2);;

let separa2 l = List.partition (fun el -> el.voto < 18) l;;

let prop_separa (xs: valV list) = 
    let (l1, l2) = separa xs
    in List.sort xs = List.sort (l1 @ l2);;

do Check.Quick prop_separa;;