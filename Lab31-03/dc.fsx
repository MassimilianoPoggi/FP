﻿type category =
    | Daycare
    | Nursery
    | Recreation;;

let categoryList = [Daycare; Nursery; Recreation];

type names = string;;

type childDes = { name: names; cat: category };;

let rec number (cat, l) = 
    match l with
        | [] -> 0
        | h::tl when h.cat = cat -> 1 + number(cat, tl)
        | h::tl -> number(cat, tl);;

let number2 (cat, l) = 
    List.filter (fun el -> el.cat = cat) l |> List.length;;

let prop_number (xs: childDes list) =
    number(Daycare, xs) + number(Nursery, xs) + number(Recreation, xs) = (List.length xs);;

let prop_number2 (xs: childDes list) =
    List.fold (fun acc x -> acc + number(x, xs)) 0 categoryList = List.length(xs);;

#r "FsCheck"
open FsCheck;;

do Check.Quick prop_number;;

let price cat =
    match cat with
        | Daycare -> 225.
        | Nursery -> 116.
        | Recreation -> 110.;;

let pay (n, l) =
    let rec pay_cat (l, c) =
        let rec pay_half_cat l =
            match l with
                | [] -> 0.
                | h::tl when h.name = n && h.cat = c -> (price h.cat / 2.) + pay_half_cat tl
                | h::tl -> pay_half_cat tl
        in match l with
            | [] -> 0.
            | h::tl when h.name = n && h.cat = c -> price h.cat + pay_half_cat tl
            | h::tl -> pay_cat (tl, c)   
    in pay_cat(l, Daycare) + pay_cat(l, Nursery) + pay_cat(l, Recreation);;

//Per ogni categoria c, se il prezzo senza sconto è x allora il prezzo con lo sconto è 
//prezzo_c + (x - prezzo_c) / 2
//se però x = 0 allora il prezzo con lo sconto rimane 0
let pay2 (n, l) = 
    let discounted_price c = 
        let normal_price c = 
            (price c) * float (List.length (List.filter (fun el -> el.cat = c && el.name = n) l)) 
        in match normal_price c with
            | 0. -> 0.
            | x -> price c + (x - (price c)) / 2. 
    in List.fold (fun acc el -> acc + discounted_price el) 0. categoryList;;

let normal_price (n, l) = 
    let normal_price c = 
        (price c) * float (List.length (List.filter (fun el -> el.cat = c && el.name = n) l))
    in List.fold (+) 0. (List.map (normal_price) categoryList);;

let prop_pay (n, l) =
    pay(n, l) <= normal_price (n, l);;

do Check.Quick prop_pay;;