﻿//NO TAIL RECURSION
let rec dotProduct v1 v2 =
    match v1, v2 with
        | [], [] -> 0
        | h1::tl1, h2::tl2 -> h1*h2 + dotProduct tl1 tl2
        | _ -> raise (System.ArgumentException("I vettori devono avere la stessa lunghezza"));;

//TAIL RECURSION
let dotProduct3 v1 v2 =
    let rec dotProduct acc v1 v2 =
        match v1, v2 with
            | [], [] -> acc
            | h1::tl1, h2::tl2 -> dotProduct (h1*h2 + acc) tl1 tl2
            | _ -> raise (System.ArgumentException("I vettori devono avere la stessa lunghezza"))
    in dotProduct 0 v1 v2;;

let dotProduct2 v1 v2 = 
    List.fold2 (fun acc h1 h2 -> acc + h1*h2) 0 v1 v2;;

#r "FsCheck"
open FsCheck;;

let prop_products v1 v2 =
    List.length v1 = List.length v2 ==> 
        lazy(dotProduct v1 v2 = dotProduct2 v1 v2 && dotProduct2 v1 v2 = dotProduct3 v1 v2);;

do Check.Quick prop_products;;

//NO TAIL RECURSION
let rec takeWhile pred list =
    match list with
        | [] -> []
        | h::tl when not (pred h) -> []
        | h::tl -> h::takeWhile pred tl;;

//TAIL RECURSION
let takeWhile2 pred list =
    let rec takeWhile acc pred list =
        match list with
            | [] -> List.rev acc
            | h::tl when not (pred h) -> List.rev acc
            | h::tl -> takeWhile (h::acc) pred tl
    in takeWhile [] pred list;;

let prop_takewhiles pred list =
    takeWhile pred list = takeWhile2 pred list;;

do Check.Quick prop_takewhiles;

let rec dropWhile pred list =
    match list with
        | [] -> []
        | h::tl when not (pred h) -> h::tl
        | h::tl -> dropWhile pred tl;;

let prop_takedrop pred l =
    l = takeWhile pred l@dropWhile pred l;;

do Check.Quick prop_takedrop;;

let safeDiv n1 n2 = 
    match n1, n2 with
        | None, _ | _, None | _, Some(0) -> None
        | Some(a), Some(b) -> Some(a/b);;

let optMapBinary f x y =
    match x, y with
        | None, _ | _, None -> None
        | Some(a), Some(b) -> Some(f a b);;

let optPlus = optMapBinary (+);;
let optTimes = optMapBinary ( *);;

type form = Const of bool | Not of form | And of form * form | Or of form * form;;

let rec eval expr = 
    match expr with
        | Const(x) -> x
        | Not(x) -> not (eval x)
        | And(x, y) -> eval x && eval y
        | Or(x, y) -> eval x || eval y;;

let rec toString expr = 
    match expr with
        | Const(x) -> string x
        | Not(x) -> "- " + toString x
        | And(x, y) -> "(" + toString x + " /\ " + toString y + ")"
        | Or(x, y) -> "(" + toString x + " \/ " + toString y + ")";;

let main expr =
    let str, res = toString expr, eval expr
    in "il risultato di " + str + " è " + (string res);;