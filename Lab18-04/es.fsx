﻿let isuml list =
    let rec sumlA l acc = 
        match l with
            | [] -> acc
            | h::tl -> sumlA tl (acc+h)
    in sumlA list 0;;

let rec suml = function
    | [] -> 0
    | x :: xs -> x + suml xs;;

#time;;

isuml [1..5000];;
suml [1..5000];;