﻿type 'a Tree = Null | Node of 'a * 'a Tree * 'a Tree;;

let rec intToFloatTree t = 
    match t with
        | Null -> Null
        | Node(el, lt, rt) -> Node(float el, intToFloatTree lt, intToFloatTree rt);;

let rec inorderToList t =
    match t with
        | Null -> []
        | Node(el, lt, rt) -> inorderToList lt @ (el::inorderToList rt);;

let rec preorderToList t =  
    match t with
        | Null -> []
        | Node(el, lt, rt) -> el::inorderToList lt @ inorderToList rt;;
   
#r "FsCheck"

open FsCheck;;   
        
let prop_visit (btree : int Tree) =
  let l1 = inorderToList btree  |> List.sort in
  let l2 = preorderToList btree |> List.sort in
  l1 = l2 ;;

Check.Quick prop_visit;;

let rec search (el, t) =
    match t with
        | Null -> false
        | Node(v, _, _) when v = el -> true
        | Node(_, lt, rt) -> search (el, lt) || search (el, rt);;

let prop_search (x: int, binTree: int Tree) =
    List.exists (fun a -> a = x) (inorderToList binTree) ==> search (x, binTree);;

Check.Quick prop_search;;

let filterToList (pred, t) = List.filter pred (inorderToList t);;

let even = (fun x -> x % 2 = 0);;

let small = (fun x -> x < 5);;

let rec count t = 
    match t with
        | Null -> (0,0)
        | Node(el, Null, Null) -> (1, 1)
        | Node(el, lt, rt) -> let (n1, l1), (n2, l2) = (count lt, count rt) in (1+n1+n2, l1+l2);;

let rec depthToList (n, t) = 
    match n, t with
        | _, Null -> []
        | 0, Node(el, _, _) -> [el]
        | n, Node(_, lt, rt) -> depthToList (n-1, lt) @ depthToList (n-1, rt);;

type direction = L | R;;

let rec getElement (d,t)  =
    match d,t with
        | _, Null -> None
        | [], Node(el, _, _) -> Some(el)
        | x::tl,Node(_, lt, _) when x = L -> getElement(tl, lt)
        | _::tl, Node(_, _, rt) -> getElement(tl, rt);;

let rec insert (n, t) =
    match t with
        | Null -> Node(n, Null, Null)
        | Node(el, lt, rt) when n = el -> Node(el, lt, rt)
        | Node(el, lt, rt) when n < el -> Node(el, insert(n, lt), rt)
        | Node(el, lt, rt)  -> Node(el, lt, insert(n, rt));;

let rec insertFromList (l, t) =
    match l with
        | [] -> t
        | h::tl -> insertFromList(tl, insert(h, t));;

let rec search1 (n, t) =
    match t with
        | Null -> false
        | Node(el, _, _) when el = n -> true
        | Node(el, lt, _) when n < el -> search1(n, lt)
        | Node(el, _, rt) -> search1(n, rt);;

let searchPath (n, t) =
    let rec sPath tree acc =
        match tree with
            | Null -> []
            | Node(el, _, _) when n = el -> List.rev (el::acc)
            | Node(el, lt, _) when n < el -> sPath lt (el::acc)
            | Node(el, _, rt) -> sPath rt (el::acc)
    in sPath t [];;

let rec min tree =
    match tree with
        | Null -> None
        | Node(el, Null, _) -> Some(el)
        | Node(el, lt, _) -> min lt;;

let rec subtree (n, t) =
    match t with
        | Null -> Null
        | Node(el,lt, rt) when n = el -> Node(el, lt, rt)
        | Node(el, lt, _) when n < el -> subtree (n,lt)
        | Node(el, _, rt) -> subtree(n, rt);;