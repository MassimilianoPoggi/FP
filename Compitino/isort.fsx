﻿let insert x ls =
    let (l1, l2) = List.partition (fun el -> el <= x) ls 
    in l1 @ (x::l2);;

let isort ls =
    List.fold (fun acc x -> insert x acc) [] ls;;

let insertBy pred x ls=
    let (l1, l2) = List.partition (fun el -> pred el x) ls
    in l1 @ (x::l2);;

let isortBy pred ls =
    List.fold (fun acc x -> insertBy pred x acc) [] ls;;

let prop_sort pred (xs: int list) =
    let pred_adapt x y = match pred x y with
        | true -> -1
        | _ -> +1
    in List.sortWith (pred_adapt) xs = isortBy pred xs;;