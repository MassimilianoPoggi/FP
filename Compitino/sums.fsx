﻿type sums =
    | C of int
    | Add of sums list;;

let rec countAdd sum =
    match sum with
        | C(_) -> 0
        | Add(l) -> List.fold (fun acc el -> acc + countAdd el) 1 l;;

let rec ev sum =
    match sum with
        | C(x) -> x
        | Add(l) -> List.fold(fun acc el -> acc + ev el) 0 l;;