﻿type colour = Blue | Green | Red

type block =
    | Coloured of colour
    | Jolly

let count bs = List.fold (fun (c,j) el -> match el with Coloured(_) -> (c+1, j) | Jolly -> (c, j+1)) (0,0) bs;;

let getColours bs = 
    bs 
        |> List.fold (fun acc el -> match el with Coloured(x) -> x::acc | Jolly -> acc) [] 
        |> Set.ofList 
        |> Set.toList;;

let repaintB f b =
    match b with
        | Jolly -> Jolly
        | Coloured(x) ->
            match f x with
                | Some(y) -> Coloured(y)
                | _ -> b;;

let repaintBs f bs = List.map (repaintB f) bs;;