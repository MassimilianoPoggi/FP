﻿let rec sift n sq = Seq.filter (fun x -> x % n <> 0) sq;;

let sq1 = Seq.initInfinite id |> sift 2 |> Seq.take 10 |> Seq.toList;;
let sq2 = Seq.initInfinite id |> sift 3 |> Seq.take 15 |> Seq.toList;;

let rec sieve sq = seq {
    yield Seq.head sq
    yield! sieve (sift (Seq.head sq) (Seq.skip 1 sq)) }

let primes = Seq.initInfinite id |> Seq.skip 2 |> sieve |> Seq.take 10 |> Seq.toList;;

let rec siftC n sq = Seq.cache (sift n sq);;

let rec sieveC sq = seq {
    yield Seq.head sq
    yield! sieveC (siftC (Seq.head sq) (Seq.skip 1 sq)) };;

let rec allFiles dir = seq {
    yield! System.IO.Directory.GetFiles dir
    yield! System.IO.Directory.GetDirectories dir |> Seq.collect allFiles };;

let rec f x k = 
    match k with
        | 0 -> 1.
        | k -> (f x (k-1)) * x / (float k);;

let apprTaylor x = 
    let rec apprTo acc index = seq {
        yield acc + (f x index)
        yield! apprTo (acc + (f x index)) (index + 1) }
    in apprTo 0. 0;;

let error_range x k = 
    match x < 0. with
        | true -> f x (k+1)
        | false -> (3.**x) * (f x (k+1));;

let apprExpr x delta =
    let rec find_index i =
        match error_range x i < delta with
            | true -> Seq.nth i (apprTaylor x)
            | false -> find_index (i+1)
    in find_index 0;;

let lazy_prod n1 n2 = 
    match n1() with
        | 0 -> (fun () -> 0)
        | a -> (fun () -> a * n2());;

(lazy_prod (fun () -> 0) (fun () -> 5/0))();;
(lazy_prod (fun () -> 5) (fun () -> 5/1))();;