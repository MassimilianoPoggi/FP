﻿let length_acc list =
    let rec length l acc =
        match l with
            | [] -> acc
            | _::tl -> length tl (acc+1)
    in length list 0;;

let length_fold list = List.fold (fun acc el -> acc+1) 0 list;;

let length_cont list =
    let rec length l cont =
        match l with
            | [] -> cont 0
            | _::tl -> length tl (fun x -> cont (x+1))
    in length list id;;

#r "FsCheck"
open FsCheck;;

let prop_lengths l =
    length_acc l = length_fold l && length_fold l = length_cont l;;

do Check.Quick prop_lengths;;

let map_acc f list =
    let rec map l acc =
        match l with
            | [] -> List.rev acc
            | h::tl -> map tl (f h::acc)
    in map list [];;

let map_cont f list =
    let rec map l cont =
        match l with
            | [] -> cont []
            | h::tl -> map tl (fun x -> cont (f h::x))
    in map list id;;

let prop_maps f l =
    map_acc f l = map_cont f l;;

do Check.Quick prop_maps;;

type 'a binTree = Empty | Node of 'a * 'a binTree * 'a binTree;;

let preorder_acc tree =
    let rec preorder tree acc =
        match tree with
            | Empty -> acc
            | Node(el, lt, rt) ->
                let ll = preorder lt (acc@[el])
                in preorder rt ll
    in preorder tree [];;

let preorder_cont tree =
    let rec preorder tree cont =
        match tree with
            | Empty -> cont []
            | Node(el, lt, rt) ->
                preorder lt (fun ll -> preorder rt (fun rl -> cont ((el::ll) @ rl)))
    in preorder tree id;;

let prep_preorders t =
    preorder_acc t = preorder_cont t;;

do Check.Quick prep_preorders;;

let inorder_acc tree =
    let rec inorder tree acc =
        match tree with
            | Empty -> acc
            | Node(el, lt, rt) -> 
                let rl = inorder rt acc
                in inorder lt (el::rl)
    in inorder tree [];;

let inorder_cont tree =
    let rec inorder tree cont =
        match tree with
            | Empty -> cont []
            | Node(el, lt, rt) -> 
                inorder lt (fun ll -> inorder rt (fun rl -> cont (ll @ (el::rl))))
    in inorder tree id;;

let prep_inorders t =
    inorder_acc t = inorder_cont t;;

do Check.Quick prep_inorders;;

let postorder_acc tree =
    let rec postorder tree acc =
        match tree with
            | Empty -> acc
            | Node(el, lt, rt) ->
                let rl = postorder rt (el::acc)
                in postorder lt rl
    in postorder tree [];;

let postorder_cont tree =
    let rec postorder tree cont =
        match tree with
            | Empty -> cont []
            | Node(el, lt, rt) ->
                postorder lt (fun ll -> postorder rt (fun rl -> cont (ll @ rl @ [el])))
    in postorder tree id;;

let prep_postorders t =
    postorder_cont t = postorder_acc t;;

do Check.Quick prep_postorders;;

let sum m n =
    let rec sum first last =
        if (first > last) then 0
        else first + sum (first+1) last
    in sum m (m+n);;

let sum_acc m n =
    let rec sum first last acc =
        if first > last then acc
        else sum (first+1) last (acc+first)
    in sum m (m+n) 0;;

let prep_sums m n = 
    sum m n = sum_acc m n;;

do Check.Quick prep_sums;;

let rec fib n =
    if (n = 0) then 0
    else 
        if (n = 1) then 1
        else fib (n-1) + fib(n-2);;

let itfib n =
    let rec fib n m1 m2 =
        if n = 0 then m1
        else 
            if n = 1 then m2
            else fib (n-1) m2 (m1+m2)
    in fib n 0 1;;

let prop_fib n =
  let smallGen n =
    Arb.filter (fun x -> x < n) Arb.from<int>
  Prop.forAll (smallGen  n )
       (fun m ->  m > 0 ==> lazy (fib m = itfib m))

do Check.Quick (prop_fib 10);;