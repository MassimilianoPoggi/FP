﻿type boolex = Const of bool | Var of char | Neg of boolex | And of boolex * boolex;;

type environment = char list;;

let rec eval expr (env: environment) =
    match expr with
        | Var(c) -> List.exists (fun x -> x = c) env
        | Const(v) -> v
        | Neg(exp) -> not (eval exp env)
        | And(exp1, exp2) -> eval exp1 env && eval exp2 env;;

type boolif = Constif of bool | Varif of char | If of boolif * boolif * boolif;;

let rec ifeval expr (env: environment) =
    match expr with
        | Varif(c) -> List.exists (fun x -> x = c) env
        | Constif(v) -> v
        | If(ex1, ex2, ex3) -> if ifeval ex1 env then ifeval ex2 env else ifeval ex3 env;;

let rec bool2if expr =
    match expr with
        | Var(c) -> Varif(c)
        | Const(v) -> Constif(v)
        | Neg(exp) -> If(bool2if exp, Constif(false), Constif(true))
        | And(ex1, ex2) -> If (bool2if ex1, bool2if ex2, Constif(false));;
        
#r "FsCheck.dll"
open FsCheck;;

let prop_bools expr env =
    eval expr env = (ifeval (bool2if expr) env);; 

do Check.Quick prop_bools;;

let nat = Seq.initInfinite id;;

let seq1 = seq {
    yield! [0;1;2;0;3;4;4;3;1]
    yield! Seq.initInfinite (fun x -> x+5) };;

let seq2 = Seq.initInfinite (fun x -> int (x/2));;

let seq3 = Seq.collect (fun x -> Seq.take (x+2) (Seq.initInfinite id)) (Seq.initInfinite id);;

let distinct sequence =
    let rec build sequence used = seq {
            if not (List.exists (fun x -> x = Seq.head sequence) used) then yield Seq.head sequence
            yield! build (Seq.skip 1 sequence) (Seq.head sequence::used)
        }
    in build sequence [];;

let isEqual n s1 s2 =
    Seq.forall2 (fun el1 el2 -> el1 = el2) (Seq.take n s1) (Seq.take n s2);;