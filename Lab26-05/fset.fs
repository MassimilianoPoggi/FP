﻿module FSet

type FSet<'a when 'a : equality> = S of ('a -> bool);;

let empty = S(fun _ -> false);;

let contains value (S f) = f value;;

let singleton element = S(fun x -> x = element)

let add el set = S(fun x -> contains x (singleton el) || contains x set);;

let union set1 set2 = S(fun x -> contains x set1 || contains x set2);;

let ofList lst = List.fold (fun acc el -> add el acc) empty lst;;